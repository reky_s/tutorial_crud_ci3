<?php
/**
 * Created by PhpStorm.
 * User: rich
 * Date: 25/10/15
 * Time: 13:37
 */

class M_user extends CI_Model{
    public function __construct()
    {
        // Call the CI_Model constructor
        parent::__construct();
    }

    public function get_all(){
        $this->db->from('user');
        $this->db->order_by('id_user','asc');
        $query=$this->db->get();
        return $query->result();
    }

    public function insert($param){
        $query=$this->db->insert('user', $param);
        return $query;
    }

    public function delete($param){
        $query=$this->db->delete('user', $param);
        return $query;
    }

    public function get_by($id){
        $this->db->from('user');
        $this->db->where('id_user',$id);
        $query=$this->db->get();
        return $query->result_array();
    }

    public function update($param,$id){
        $this->db->set($param);
        $this->db->where('id_user',$id);
        $query=$this->db->update('user');
        return $query;
    }

}
