<html>
<head>
    <title>Form Data</title>
    <link href="<?=base_url('assets/bootstrap-3.3.5-dist/css/bootstrap.css')?>" type="text/css" rel="stylesheet"/>
</head>
<body>
<div class="row">
    <div class="col-md-6 col-lg-offset-3">
        <?php if(validation_errors()) { ?>
            <div class="alert alert-warning">
                <?php echo validation_errors(); ?>
            </div>
        <?php } ?>


        <?php if($this->session->flashdata('item')) { ?>
            <div class="alert alert-success">
                <?php echo $this->session->flashdata('item'); ?>
            </div>
        <?php }else if($this->session->flashdata('invalid')){
            ?>
            <div class="alert alert-danger">
                <?php echo $this->session->flashdata('invalid'); ?>
            </div>
        <?php
        } ?>

        <h3 class="text-center">Input Data</h3>
        <form action="" method="post">
            <div class="form-group">
                <div class="col-md-4">
                    <label>Nama</label>
                </div>
                <div class="col-md-8">
                    <?php  $resNama = isset($data[0]['nama']) ? $data[0]['nama'] : ''; ?>
                    <input type="text" name="nama" class="form-control" value="<?php echo set_value('nama',$resNama); ?>"><br/>
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-4">
                    <label>Alamat</label>
                </div>
                <div class="col-md-8">
                    <?php $resAlamat = isset($data[0]['alamat']) ? $data[0]['alamat']: ''; ?>
                    <input type="text" name="alamat" class="form-control" value="<?php echo set_value('alamat',$resAlamat); ?>"><br/>
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-12 text-right">
                    <button type="submit" class="btn btn-primary">Simpan</button>
                </div>
            </div>
        </form>
        <div class="col-md-12 text-left">
            <a href="<?=base_url('welcome/read')?>" class="btn btn-success">View</a>
        </div>
    </div>
</div>
</body>
</html>