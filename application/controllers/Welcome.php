<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
    public function __construct(){
        parent::__construct();
        $this->load->model('M_user');
        $this->load->library(array('session'));
    }
	public function index()
	{
		$this->load->view('welcome_message');
	}


    public function read(){
        $data=array('result'=>$this->M_user->get_all());
        $this->load->view('view',$data);
    }

    public function create(){
        $this->load->library(array('Form_validation'));
        $this->load->helper(array('form'));

        $this->form_validation->set_rules('nama', 'Nama', 'trim|required|min_length[2]|max_length[12]');
        $this->form_validation->set_rules('alamat', 'Alamat', 'trim|required|min_length[3]');
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');

        if ($this->form_validation->run() == FALSE) {
            $this->load->view('form');
        }else{
            $data=array('nama'=>$this->input->post('nama'),'alamat'=>$this->input->post('alamat'));
            if($this->M_user->insert($data)){
                $this->session->set_flashdata('item', 'form submitted successfully');
            }else{
                $this->session->set_flashdata('invalid', 'form submitted failed');
            }
            redirect(current_url());
        }
    }

    public function delete($id){
        $data=array('id_user'=>$id);
        if($this->M_user->delete($data)){
            $this->session->set_flashdata('item', 'delete success');
            redirect('welcome/read');
        }else{
            $this->session->set_flashdata('invalid', 'delete failed');
            redirect('welcome/read');
        }
    }

    public function update($id){
        $this->load->library(array('Form_validation'));
        $this->load->helper(array('form'));

        $this->form_validation->set_rules('nama', 'Nama', 'trim|required|min_length[2]|max_length[12]');
        $this->form_validation->set_rules('alamat', 'Alamat', 'trim|required|min_length[3]');
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');

        if ($this->form_validation->run() == FALSE) {
            if($this->M_user->get_by($id)){
                $data['data']=$this->M_user->get_by($id);
                $this->load->view('form',$data);
            }else{
                $this->session->set_flashdata('invalid', 'data not found');
                $this->load->view('form');
            }
        }else{
            $data=array('nama'=>$this->input->post('nama'),'alamat'=>$this->input->post('alamat'));
            if($this->M_user->update($data,$id)){
                $this->session->set_flashdata('item', 'form submitted successfully');
            }else{
                $this->session->set_flashdata('invalid', 'form submitted failed');
            }
            redirect(current_url());
        }
    }

}
